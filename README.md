# DevOps final project
## project silos
~~~
The project will include those silos:
1. Infrastructures silo
2. Application silo
3. Pipelines silo
~~~



## Explanation about the infrastructures silo
~~~
1. Kubernetes cluster establishment

2. Jenkins server establishment

3. ArgoCD solution establishment

4. Prometheus and Grafana establishment

5. PostgreSQL database establishment
~~~
![Example Image](infra.png)



## Explanation about the application silo
~~~
1. The application written in Python3

2. Dockerize the application

3. add unit-test

4. use a Relational database
~~~
![Example Image](app.png)



## Explanation about the Pipelines silo
~~~
Multi branch pipeline with two branches(main and feature)
The Main branch will be pushed to Jenkins & Dockerhub and the ArgoCD will pull it and run the application
~~~
![Example Image](cicd.png)