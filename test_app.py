import pytest
from flask import Flask
from database import db, get_all_contacts, contacts

@import pytest
from flask import Flask
from database import db, get_all_contacts, contacts

@pytest.fixture
def app():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)

    with app.app_context():
        db.create_all()

    yield app

    with app.app_context():
        db.session.remove()
        db.drop_all()

@pytest.fixture
def client(app):
    return app.test_client()

def test_get_contacts_lists(app, client):
    with app.app_context():
        contacts1 = contacts(first_name='user', last_name='one', description='this is user one', phone='1234567890')
        contacts2 = contacts(first_name='user', last_name='two', description='this is user two', phone='1234567')


        db.session.add(contacts1)
        db.session.add(contacts2)
        db.session.commit()



        all_contacts = get_all_contacts()
        assert len(all_contacts) == 2
